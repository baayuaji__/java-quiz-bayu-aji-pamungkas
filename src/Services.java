import java.util.ArrayList;
import java.util.List;

public class Services {


    //1. MENGHITUNG VOUCHER TERTINGGI
    public static List<Integer> getHighest(Integer userPoint){
        List<Integer> highest = new ArrayList<>();

        if(userPoint >= 800){
            highest.add(100000);
            highest.add(800);
            return highest;
        } else if (userPoint >= 400){
            highest.add(50000);
            highest.add(400);
            return highest;
        } else if (userPoint >= 200) {
            highest.add(25000);
            highest.add(200);
            return highest;
        } else if (userPoint >= 100){
            highest.add(10000);
            highest.add(100);
            return highest;
        } else {
            highest.add(0);
            highest.add(0);
            return highest;
        }
    }


    //2. MENGHITUNG SISA POIN
    public static Integer countRemainingPoint(Integer myPoint){
        return  myPoint - getHighest(myPoint).get(1);
    }


    //3. REDEEM SEMUA POIN
    /*
        1. pertama, buat variable penampung hasil
        3. redeem terhadap voucher tertinggi
        4. masukkan hadiah voucher ke penampung
        5. update total poin tersisa
        5. ulangi proses selama poin tersisa lebih besar atau sama dengan (>=) 100
        6. masukkan sisa poin ke penampung hasil
        7. return hasil
    */
    public static List<Integer> redeemAllPoints(Integer myPoint){
        if(myPoint <100){return List.of(0);}
        List<Integer> results = new ArrayList<>();

        while (myPoint >= 100) {
            results.add(getHighest(myPoint).get(0));
            myPoint = countRemainingPoint(myPoint);
        }
        results.add(myPoint);
        return results;
    }
}
