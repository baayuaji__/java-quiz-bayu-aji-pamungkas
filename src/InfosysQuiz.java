import java.util.*;

public class InfosysQuiz {
    private static final String line = "=======================";

    public static void main(String[] args) {

        //1. menentukan voucher dengan poin tertinggi
        Integer myPoint = 1000;
        System.out.println("\n" + line);
        System.out.println("1. HIGHEST VOUCHER YOU CAN REDEEM\n");
        System.out.println("Total poin anda: " + myPoint + "p");
        List<Integer> voucher = Services.getHighest(myPoint);
        System.out.println("voucher tertinggi yang dapat di redeem: Rp" + voucher.get(0) + ", seharga " + voucher.get(1) + "p\n");


        //2. menghitung sisa poin setelah di redeem dengan poin tertinggi
        System.out.println("-----------------------");
        System.out.println("2. YOUR REMAINING POINTS\n");
        System.out.println("sisa poin setelah di redeem dengan poin tertinggi: " + Services.countRemainingPoint(myPoint) + "p\n");


        //3. menampilkan voucher yang didapat jika redeem all points
        Integer myPoint2 = 1150;

        System.out.println(line);
        System.out.println("3. REDEEM ALL YOUR POINTS\n");
        System.out.println("Total poin awal anda: " + myPoint2 + "p");

        if(myPoint2 < 100){
            System.out.println("Maaf poin anda kurang! Mohon kumpulkan lebih banyak untuk redeem poin");
        } else {
            List<Integer> listVouchers = Services.redeemAllPoints(myPoint2);
            System.out.println("Total sisa poin akhir: " + listVouchers.get(listVouchers.size()-1) + "p\n");
            listVouchers.remove(listVouchers.size()-1);

            System.out.println("===== total voucher yang didapat dari redeem semua poin =====");
            Set<Integer> setVouchers = new HashSet<>(listVouchers);
            for (Integer distinctVoucher : setVouchers) {
                System.out.println("voucher Rp" + distinctVoucher + ":  sebanyak " + Collections.frequency(listVouchers, distinctVoucher) + "x");
            }

            System.out.println(line);
        }

    }
}